import tensorflow as tf

str_to_activation_function = {
    "relu": tf.nn.relu,
    "sigmoid": tf.nn.sigmoid,
    "softmax": tf.nn.softmax,
    "softplus": tf.nn.softplus,
    "tanh": tf.nn.tanh,
}