import os
import re
from datetime import datetime
from threading import Thread
from zipfile import ZipFile

import cv2
import numpy as np
from kivy.app import App
from kivy.clock import mainthread
from kivy.properties import ObjectProperty, StringProperty
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.dropdown import DropDown
from kivy.uix.floatlayout import FloatLayout
from kivy.uix.label import Label
from kivy.uix.popup import Popup
from kivy.uix.recycleview import RecycleView
from kivy.uix.textinput import TextInput
from kivy.config import Config
from tensorflow.python.keras.callbacks import LambdaCallback

from core.maps import loss_map
from core.neuraltrainer import NeuralTrainer
from utils import str_to_activation_function

Config.set("graphics", "width", 1000)
Config.set("graphics", "height", 600)
Config.write()


def start_learning(app, input_size, layers, neurons, activations, output_neurons, output_activation, dense_neurons,
                   optimizer, loss_function, epochs, vsplit):
    acc = 0.0
    loss = 0.0
    current_epoch = 0

    def update_epoch(epoch, logs):
        nonlocal current_epoch
        current_epoch = epoch

    @mainthread
    def update_text(text):
        app.popup_label.text = text

    def batch_end(batch, logs):
        nonlocal acc, loss
        if 'acc' in logs and 'loss' in logs:
            acc = logs.get('acc')
            loss = logs.get('loss')
        if app.popup_label is not None and batch % 3 == 0:
            update_text(f"Epoch {current_epoch + 1}/{epochs} ({logs.get('size') * batch}/{train_elements})\n"
                        f"loss = {loss:6.3f}, accuracy = {acc:6.3f}")

    @mainthread
    def finish_popup(model_name):
        app.create_popup("Model trained", f"Model has been trained and saved in {model_name}.zip")

    def train_end(logs):
        app.msg_popup.dismiss()
        model_name = "model_" + datetime.now().strftime("%Y_%m_%d_%H_%M")
        neural_trainer.save_model(model_name)
        finish_popup(model_name)

    neural_trainer = NeuralTrainer(input_shape=(input_size[0], input_size[1], 1),
                                   layers=layers,
                                   neurons=neurons,
                                   activations=activations,
                                   dense_neurons=dense_neurons,
                                   output_neurons=output_neurons,
                                   output_activation=output_activation,
                                   optimizer=optimizer,
                                   loss=loss_function)

    X_train = []
    y_train = []
    with ZipFile(app.ZIPFILE) as z:
        for e in app.FILELIST:
            y_train.append(re.search(r"(\d+)_\w+.(jpg|png)", e.filename).group(1))
            data = z.read(e)
            img = cv2.imdecode(np.frombuffer(data, np.uint8), 1)
            img = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
            img = cv2.resize(img, input_size)
            X_train.append(img)
    vsplit /= 100.0
    train_elements = int(len(X_train) * (1 - vsplit))

    neural_trainer.set_learning_data(X_train, y_train)
    neural_trainer.add_callback(LambdaCallback(on_epoch_begin=update_epoch, on_batch_end=batch_end,
                                               on_train_end=train_end))
    neural_trainer.learn(epochs, vsplit)


class HiddenLayersList(RecycleView):
    HIDDEN_LAYERS = 2

    def update_list(self, text):
        try:
            layers = int(text)
        except ValueError:
            return
        self.HIDDEN_LAYERS = layers
        self.data = [{"text": "0"} for _ in range(self.HIDDEN_LAYERS)]


class HiddenLayersTextInput(TextInput):
    pass


class HiddenLayersColumn(BoxLayout):
    pass


class ActivationDropDown(DropDown):
    pass


class OptimizerDropDown(DropDown):
    pass


class LossDropDown(DropDown):
    pass


class Window(BoxLayout):
    hidden_layers_list = ObjectProperty(None)
    filelistview = ObjectProperty(None)
    activation_button = ObjectProperty(None)
    output_activation_button = ObjectProperty(None)
    loss_button = ObjectProperty(None)
    optimizer_button = ObjectProperty(None)
    img_width = ObjectProperty(None)
    img_height = ObjectProperty(None)
    output_neurons = ObjectProperty(None)
    dense_neurons = ObjectProperty(None)
    num_of_epochs = ObjectProperty(None)
    validation_split = ObjectProperty(None)
    total_files = StringProperty()
    ZIPFILE = None
    FILELIST = []

    def __init__(self, **kwargs):
        super(Window, self).__init__(**kwargs)
        self.popup_label = None
        self.msg_popup = None
        self.total_files = str("Total files: 0")

    def dismiss_popup(self):
        self._popup.dismiss()

    def show_load(self):
        content = LoadDialog(load=self.load, cancel=self.dismiss_popup)
        self._popup = Popup(title="Load file", content=content,
                            size_hint=(1, 1))
        self._popup.open()

    def load(self, path, filename):
        if len(filename) == 1 and filename[0].endswith("zip"):
            self.load_zip(os.path.join(path, filename[0]))

        self.dismiss_popup()

    def load_zip(self, path):
        print(f"Loading zip {path}")
        self.ZIPFILE = path
        with ZipFile(path) as train_data:
            self.FILELIST = [entry for entry in train_data.filelist
                             if re.search(r"(\d+)_\w+.(jpg|png)", entry.filename) is not None]
        self.filelistview.update_files(self.FILELIST)
        self.total_files = f"Total files: {len(self.FILELIST)}"

    def train(self):
        try:
            self.valid_data()
        except AssertionError as e:
            self.create_popup("Error", str(e))
            return
        hidden_neurons = list(map(lambda x: int(x.text), self.hidden_layers_list.children[0].children))
        activations = [str_to_activation_function.get(self.activation_button.text.lower())] * len(hidden_neurons)
        output_activation = str_to_activation_function.get(self.output_activation_button.text.lower())
        loss = loss_map.get(self.loss_button.text)

        args = (self, (int(self.img_width.text), int(self.img_height.text)), self.hidden_layers_list.HIDDEN_LAYERS,
                hidden_neurons, activations, int(self.output_neurons.text), output_activation,
                int(self.dense_neurons.text), self.optimizer_button.text.lower(), loss, int(self.num_of_epochs.text),
                int(self.validation_split.text))
        self.create_popup("Training", "Preparing data...", auto_dismiss=False)
        learn_thread = Thread(target=start_learning, args=args)
        learn_thread.start()

    def valid_data(self):
        assert 1 < self.hidden_layers_list.HIDDEN_LAYERS < 9, "Number of convolutional layers must be between 1 and 8"
        try:
            neurons = list(map(lambda x: int(x.text), self.hidden_layers_list.children[0].children))
            for neuron in neurons:
                assert neuron > 0, "Each layer must have atleast one neuron"
        except ValueError:
            raise AssertionError("Input of each layer must be integer")
        assert self.activation_button.text != "Choose", "You must choose activation function for hidden layers"
        try:
            assert int(self.output_neurons.text) > 0, "Number of output neurons must be greater than 0"
        except ValueError:
            raise AssertionError("Number of output neurons must be integer")
        try:
            assert int(self.dense_neurons.text) > 0, "Number of dense layer neurons must be greater than 0"
        except ValueError:
            raise AssertionError("Number of dense layer neurons must be integer")
        assert self.output_activation_button.text != "Choose", "You must choose output layer activation function"
        assert self.loss_button.text != "Choose", "You must choose loss function"
        assert self.optimizer_button.text != "Choose", "You must choose optimizer"
        try:
            assert int(self.img_width.text) > 0, "Input width must be greater than 0"
            assert int(self.img_height.text) > 0, "Input height must be greater than 0"
        except ValueError:
            raise AssertionError("Input width and height must be integer")
        try:
            assert int(self.num_of_epochs.text) > 0, "Number of epochs must be greater than 0"
        except ValueError:
            raise AssertionError("Number of epochs must be integer")
        try:
            vsplit = int(self.validation_split.text)
            assert 0 <= vsplit <= 100, "Validation split must be from range 0 - 100"
        except ValueError:
            raise AssertionError("Validation split must be a number")
        assert len(self.FILELIST) > 0, "You must select training files"

    def create_popup(self, title, content, auto_dismiss=True):
        self.popup_label = Label(text=content)
        self.msg_popup = Popup(title=title,
                               content=self.popup_label,
                               size_hint=(.5, .3),
                               auto_dismiss=auto_dismiss)
        self.msg_popup.open()


class TrainingFileListView(BoxLayout):
    def update_files(self, file_list):
        self.clear_widgets()
        for entry in file_list[:15]:
            self.add_widget(Label(text=entry.filename))


class LoadDialog(FloatLayout):
    load = ObjectProperty(None)
    cancel = ObjectProperty(None)


class NeuralTrainerApp(App):
    def build(self):
        return Window()


if __name__ == "__main__":
    NeuralTrainerApp().run()
