import png
import tensorflow as tf
import os


mnist = tf.keras.datasets.mnist
(x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()
print(x_train[0])
print(y_train[0])
directory = os.path.dirname(__file__)
nums = [0] * 10
for x, y in zip(x_train, y_train):
    nums[y] += 1
    png.from_array(x, 'L').save(directory + f"/datasets/mnist/{y}_{nums[y]}.png")
