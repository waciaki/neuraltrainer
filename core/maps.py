loss_map = {
    "Binary Crossentropy": "binary_crossentropy",
    "Categorical Crossentropy": "categorical_crossentropy",
    "Sparse Categorical Crossentropy": "sparse_categorical_crossentropy",
    "Mean Absolute Error": "mean_absolute_error",
    "Mean Absolute Percentage Error": "mean_absolute_percentage_error",
    "Mean Squared Error": "mean_squared_error",
    "Mean Squared Logarithmic Error": "mean_squared_logarithmic_error"
}
