import json
import os
import random
import zipfile

import numpy as np
import tensorflow as tf

keras = tf.keras

np.set_printoptions(edgeitems=30, linewidth=100000,
                    formatter=dict(float=lambda x: "%5.2g" % x))


class NeuralTrainer:
    def __init__(self, input_shape, layers, neurons, activations, dense_neurons, output_neurons, output_activation,
                 optimizer, loss):
        if layers != len(neurons):
            raise Exception("Length of neuron list must be equal to number of layers")
        if layers != len(activations):
            raise Exception("Length of activation function list must be equal to number of layers")

        self.learning_data = []
        self.callbacks = []
        self.input_shape = input_shape

        self.model = keras.models.Sequential()

        self.model.add(keras.layers.Conv2D(neurons[0], (3, 3), input_shape=self.input_shape, activation=activations[0]))
        self.model.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))

        for i in range(1, layers):
            self.model.add(keras.layers.Conv2D(neurons[i], (3, 3), activation=activations[i]))
            self.model.add(keras.layers.MaxPooling2D(pool_size=(2, 2)))

        self.model.add(keras.layers.Flatten())
        self.model.add(keras.layers.Dense(dense_neurons))
        self.model.add(keras.layers.Dense(output_neurons, activation=output_activation))

        self.model.compile(loss=loss,
                           optimizer=optimizer,
                           metrics=["accuracy"])

    def save_model(self, name):
        json_model = self.model.to_json()
        with open(f"{name}.json", "w") as f:
            f.write(json_model)
        self.model.save_weights(f"{name}.h5")
        config = {
            "width": self.input_shape[0],
            "height": self.input_shape[1]
        }
        with open(f"{name}.cfg", "w") as f:
            json.dump(config, f)

        zipf = zipfile.ZipFile(f"{name}.zip", "w", zipfile.ZIP_DEFLATED)
        zipf.write(f"{name}.json")
        zipf.write(f"{name}.h5")
        zipf.write(f"{name}.cfg")
        os.remove(f"{name}.cfg")
        os.remove(f"{name}.h5")
        os.remove(f"{name}.json")
        zipf.close()

        # self.model.save(f"{name}.model")

    def load_model(self, path):
        self.model = keras.models.load_model(path)

    def add_callback(self, callback):
        self.callbacks.append(callback)

    def remove_callback(self, callback):
        self.callbacks.remove(callback)

    def set_learning_data(self, inputs, outputs):
        self.learning_data.clear()
        if len(inputs) != len(outputs):
            raise Exception("Length of inputs is not equal to length of outputs")
        for i in range(len(inputs)):
            self.learning_data.append((inputs[i], outputs[i]))

    def append_data(self, inp, out):
        self.learning_data.append((inp, out))

    def prepare_data(self):
        random.shuffle(self.learning_data)
        inputs = np.array([x for x, y in self.learning_data])
        inputs = inputs.reshape([-1, self.input_shape[0], self.input_shape[1], 1])
        outputs = np.array([y for x, y in self.learning_data])
        inputs = inputs / 255  # keras.utils.normalize(inputs, axis=1)
        return inputs, outputs

    def learn(self, epochs, vsplit):
        inputs, outputs = self.prepare_data()
        print(inputs.shape)
        self.model.fit(inputs, outputs, epochs=epochs, callbacks=self.callbacks, validation_split=vsplit)

    def predict(self, data):
        return self.model.predict(data)
