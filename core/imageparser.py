import cv2


class ImageParser:
    @staticmethod
    def load_grayscale(path, size=None, threshhold=None):
        img = cv2.imread(path, 0)
        if size:
            img = cv2.resize(img, size)
        if threshhold:
            _, img = cv2.threshold(img, threshhold, 255, cv2.THRESH_BINARY_INV)
        return img


if __name__ == "__main__":
    img = ImageParser.load_grayscale("../img/1_2.png", (28, 28))
