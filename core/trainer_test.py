import random
import numpy as np
import tensorflow as tf
import matplotlib.pyplot as plt
from core.imageparser import ImageParser
from core.neuraltrainer import NeuralTrainer


USE_EXISTING_MODEL = True

# Load dataset of hand-written digits
(x_train, y_train), (x_test, y_test) = tf.keras.datasets.mnist.load_data()
asd = x_train[0].flatten()
neural_trainer = NeuralTrainer(input_dim=784,
                               layers=2,
                               neurons=[128, 128],
                               activations=[tf.nn.relu, tf.nn.relu],
                               output_neurons=10,
                               output_activation=tf.nn.softmax)

if USE_EXISTING_MODEL:
    # Load saved model
    neural_trainer.load_model("test_model.model")
else:
    # Learn network and save model
    neural_trainer.set_learning_data(x_train, y_train)
    neural_trainer.learn(epochs=3)
    neural_trainer.save_model("test_model")

# neural_trainer.save_model("test_model")
# # Random digit
# rand_index = random.randint(0, len(x_test))
# # Print drawn number
# print(f"Random = {y_test[rand_index]}")
# # Show drawn number
# plt.imshow(x_test[rand_index], cmap=plt.cm.binary)
# plt.show()
# # Predict drawn number and print prediction
# predictions = neural_trainer.predict(x_test[rand_index])
# print("Predicted = {}".format(np.argmax(predictions[0])))


img = ImageParser.load_grayscale("../img/test.png", size=(28, 28))  # , threshhold=127)
print(img)
predictions = neural_trainer.predict(img/255)
print(predictions)
print("Predicted = {}".format(np.argmax(predictions[0])))
